session:answer();
session:streamFile("TrimesterIntro.wav");
local i = 0; 
while (session:ready() == true) do
    session:setAutoHangup(false);
    ---Play trimester information according to the user input
    digits = session:getDigits(1, "", 10000);
	
	---If pressed 1 then play 1st trimester information
    if (digits == "1")  then
		session:streamFile("trimester_1_info.wav");
    end
	
	---If pressed 2 then play 2nd trimester information 
    if (digits == "2")  then
		session:streamFile("trimester_2_info.wav");
    end
	
	---If pressed 3 then play 3rd trimester information
    if (digits == "3")  then
        session:streamFile("trimester_3_info.wav");
    end
   
   
   ---If pressed 0 end the call
	if(digits == "0" )then
	session:hangup();
	end
	
	---if any other or no digit digit pressed repeat the menu  three times 
	---hangup if the user has entered no digit even after the third time menu has been repeated 
		
    if ( digits ~= "1" or digits ~= "2" or digits ~= "0" )  then
		if(i==3) then
			session:hangup();
		end	
		i=i+1;
		session:streamFile("Rah_e_Mah_Greeting.wav");
        digits = session:getDigits(1, "", 10000);
    end
end