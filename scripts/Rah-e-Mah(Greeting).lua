session:answer();
session:streamFile("Rah_e_Mah_Greeting.wav");
local i = 0; 
while (session:ready() == true) do
    session:setAutoHangup(false);
    
	---After greeting get input : if user wants to listen to doctor advise transfer to 2911 extension(Give trimester information)
	-- if user wants to listen to father's stories play "Father_Story.wav" audio file `
	
	--GetDigits has three arguments: max_digits, terminators, timeout
    digits = session:getDigits(1, "", 10000);
	---If pressed 1 then transfer user to trmisester information 
    if (digits == "1")  then
       session:execute("transfer","2911");
    end
	
	---If pressed 2 then play father's story 
    if (digits == "2")  then
		session:streamFile("Father_Story.wav");
    end
	
	---If pressed 0 end the call
	if(digits == "0" )then
	session:hangup();
	end
	
	---if any other or no digit digit pressed repeat the menu  three times 
	---hangup if the user has entered no digit even after the third time menu has been repeated 
		
    if ( digits ~= "1" or digits ~= "2" or digits ~= "0" )  then
		if(i==3) then
			session:hangup();
		end	
		i=i+1;
		session:streamFile("Rah_e_Mah_Greeting.wav");
        digits = session:getDigits(1, "", 10000);
    end
end